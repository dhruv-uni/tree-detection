import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
from rasterio.plot import show
import glob
import geopandas as gpd
import pandas as pd
import rasterio
from shapely.geometry import Point
import os
import json

from tqdm import tqdm

# Step 1: Read the CSV file containing the points
csv_path = 'datasets/tree_catastre_KL_raw/Baumstandorte_KL_epsg4326.csv'
points_df = pd.read_csv(csv_path, delimiter=";")
points_gdf = gpd.GeoDataFrame(points_df, geometry=gpd.points_from_xy(points_df.longitude, points_df.latitude), crs='EPSG:4326')


csv_folder = 'datasets/tree_catastre_KL/csv'
image_folder = 'datasets/tree_catastre_KL/images'

# read labelled data

"""
json_file = 'output.json'
with open(json_file, 'r') as f:
    data = json.load(f)
point_id = 1
gdf = gpd.GeoDataFrame(columns=['point_id', 'geometry'])
points_labelled = []
points_unlabelled = []
for item in tqdm(data):
    with rasterio.open(os.path.join(image_folder, os.path.basename(item['filename']))) as src:
        for i, annotation in enumerate(item['annotations']):
            x, y = src.xy(annotation['y'], annotation['x'])
            if 'unlabeled' in item and item['unlabeled']:
                points_unlabelled.append({
                'point_id': point_id,
                'x': x,
                'y': y,
                'pixel_x': round(annotation['y']),
                'pixel_y': round(annotation['x']),
                'geometry': Point(x, y)
            })
            else:
                points_labelled.append({
                    'point_id': point_id,
                    'x': x,
                    'y': y,
                    'pixel_x': round(annotation['y']),
                    'pixel_y': round(annotation['x']),
                    'geometry': Point(x, y)
                })
            point_id += 1

gdf_labelled = gpd.GeoDataFrame(points_labelled)
gdf_unlabelled = gpd.GeoDataFrame(points_unlabelled)
gdf_labelled.to_file('datasets/tree_catastre_KL/gdf_labelled.shp', driver='ESRI Shapefile')
gdf_unlabelled.to_file('datasets/tree_catastre_KL/gdf_unlabelled.shp', driver='ESRI Shapefile')
"""

gdf_labelled = gpd.read_file('datasets/tree_catastre_KL/gdf_labelled.shp')
gdf_unlabelled = gpd.read_file('datasets/tree_catastre_KL/gdf_unlabelled.shp')

# # Step 1b: Read the shapefile
#shapefile_path = 'datasets/tree_catastre_KL_raw/Gemarkungen_RLP.shp'
#data = gpd.read_file(shapefile_path)
#search_string = 'Kaiserslautern'
#kaiserlautern_shape = data[data['gemarkung'].str.contains(search_string)]

# Step 1c: Read the shapefile
shapefile_path = 'datasets/tree_catastre_KL_raw/FL Statistischer Bezirk.shp'
kl_shapes = gpd.read_file(shapefile_path)

# Create a new figure
fig, ax = plt.subplots()

# Step 2: Plot the TIFFs
folder_path = 'datasets/tree_catastre_KL_raw/'
files = glob.glob(os.path.join(folder_path, '*.tif'))

# tqdm is wrapped around the list of files
for file in tqdm(files, desc="Processing TIFFs"):
    with rasterio.open(file) as src:
        # Reproject the points to the TIFF's CRS
        crs = src.crs
        # Plot the TIFF
        show(src, ax=ax)
    #break
# Step 3: Plot the points on top of the TIFFs
#gdf = gdf.to_crs(crs)
gdf_labelled.plot(ax=ax, markersize=15, facecolor='none', edgecolor='yellow', linewidth=1)

points_gdf_reprojected = points_gdf.to_crs(crs)
points_gdf_reprojected.plot(ax=ax, markersize=10, color='red')

gdf_unlabelled.plot(ax=ax, markersize=15, facecolor='cyan', linewidth=1, marker='x', color='green')

## # Step 4: Plot the shape on top of the TIFFs
#shape_gdf_reprojected = kaiserlautern_shape.to_crs(crs)
#shape_gdf_reprojected.plot(ax=ax, facecolor="none", edgecolor='green')

# Combine the statistical districts into a single polygon
merged_polygon = kl_shapes.unary_union

# Extract the exterior boundaries of the polygon
outer_boundary = merged_polygon.exterior

# Create a GeoDataFrame with the outermost boundary
boundary_gdf = gpd.GeoDataFrame(geometry=[outer_boundary], crs=crs)
boundary_gdf.plot(ax=ax, facecolor="none", edgecolor='blue')

kl_shapes.plot(ax=ax, facecolor="none", edgecolor='green')

# Add legend
legend_elements = [Line2D([0], [0], marker='o', color='yellow', label='New Labels', markerfacecolor='none', markeredgecolor='yellow',
                         markersize=10, linestyle='None'),
                  Line2D([0], [0], marker='o', color='red', label='GT Labels',
                           markersize=10, linestyle='None'),
                  Line2D([0], [0], marker='x', color='cyan', label='GT Labels from patches with no new labels',
                          markersize=10, linestyle='None')]
ax.legend(handles=legend_elements)

plt.show()