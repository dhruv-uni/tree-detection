import os
import math
import glob
import numpy as np
import rasterio
from rasterio.windows import Window

input_folder = "tree_catastre_KL"
output_folder = "tree_catastre_KL_prep/images"
num_tiles_x = 4  # Number of tiles in the x dimension
num_tiles_y = 4  # Number of tiles in the y dimension

# Create the output folder if it does not exist
os.makedirs(output_folder, exist_ok=True)

# Iterate through the GeoTIFF files in the input folder
for input_geotiff in glob.glob(os.path.join(input_folder, "*.tif")):
    with rasterio.open(input_geotiff) as src:
        width, height = src.width, src.height
        crs = src.crs
        transform = src.transform
        count = src.count
        dtype = src.dtypes[0]

        # Calculate the tile size in each dimension
        tile_size_x = math.ceil(width / num_tiles_x)
        tile_size_y = math.ceil(height / num_tiles_y)

        # Iterate through the tiles
        for tile_y in range(num_tiles_y):
            for tile_x in range(num_tiles_y):
                # Calculate the window for the current tile
                x_start = tile_x * tile_size_x
                y_start = tile_y * tile_size_y
                window = Window(x_start, y_start, tile_size_x, tile_size_y)

                # Read the data for the current tile
                data = src.read(window=window)

                # Calculate the new transform for the current tile
                new_transform = rasterio.windows.transform(window, transform)

                # Create the output file path
                input_file_name = os.path.basename(input_geotiff)
                input_file_base, _ = os.path.splitext(input_file_name)
                output_file = os.path.join(output_folder, f"{input_file_base}_tile_{tile_x}_{tile_y}.tif")

                # Write the current tile to a new GeoTIFF file
                with rasterio.open(
                    output_file,
                    "w",
                    driver="GTiff",
                    height=window.height,
                    width=window.width,
                    count=count,
                    dtype=dtype,
                    crs=crs,
                    transform=new_transform,
                ) as dst:
                    dst.write(data)