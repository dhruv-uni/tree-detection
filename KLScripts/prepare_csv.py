import os
from pathlib import Path

import rasterio
from shapely.geometry import box
import geopandas as gpd
import pandas as pd
from tqdm import tqdm

# Step 1: Extract the bounding boxes and required information of all TIFF files
folder_path = 'tree_catastre_KL'

tiff_info = []

for file in os.listdir(folder_path):
    if file.endswith('.tif'):
        file_path = os.path.join(folder_path, file)
        with rasterio.open(file_path) as src:
            bounds = src.bounds
            crs = src.crs
            transform = src.transform

        bbox = box(*bounds)
        tiff_info.append({
            'file': file,
            'bbox': bbox,
            'crs': crs,
            'transform': transform
        })

# Step 2: Read the CSV file containing the points
csv_path = 'tree_catastre_KL/Baumstandorte_KL_epsg4326.csv'
points_df = pd.read_csv(csv_path, delimiter=";")
points_gdf = gpd.GeoDataFrame(points_df, geometry=gpd.points_from_xy(points_df.longitude, points_df.latitude), crs='EPSG:4326')

# Project points to the TIFFs' CRS
points_gdf_reprojected = points_gdf.to_crs(crs)

# Step 3: Check which points are within the bounding boxes and store the information
results = {}
points_outside = []

for i, point in tqdm(points_gdf_reprojected.iterrows(), total=len(points_gdf_reprojected), desc="Processing points"):
    is_inside_bbox = False
    for tiff in tiff_info:
        if point.geometry.within(tiff['bbox']):
            is_inside_bbox = True
            if tiff['file'] not in results:
                results[tiff['file']] = []

            # Get pixel coordinates
            px, py = rasterio.transform.rowcol(tiff['transform'], point.geometry.x, point.geometry.y)

            results[tiff['file']].append({
                'point_id': i,  # Use the row index as the generic ID
                'x': point.geometry.x,
                'y': point.geometry.y,
                'pixel_x': px,
                'pixel_y': py
            })
            break

    if not is_inside_bbox:
        points_outside.append({
            'point_id': i,
            'x': points_gdf.iloc[i].geometry.x,
            'y': points_gdf.iloc[i].geometry.y
        })

# Save the results to separate CSV files
output_folder = Path('tree_catastre_KL_prep/csv')
if not output_folder.exists():
    os.makedirs(output_folder)
for tiff_file, points in results.items():
    points_df = pd.DataFrame(points)
    output_file = os.path.join(output_folder, f'{os.path.splitext(tiff_file)[0]}_points.csv')
    points_df.to_csv(output_file, index=False)

# Save the points outside any bounding box to a CSV file
points_outside_df = pd.DataFrame(points_outside)
points_outside_df.to_csv('tree_catastre_KL_prep/csv/points_outside.csv', index=False)