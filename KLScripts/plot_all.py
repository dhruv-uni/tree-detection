import matplotlib.pyplot as plt
from rasterio.plot import show
import glob
import geopandas as gpd
import pandas as pd
import rasterio
import os

from tqdm import tqdm

# Step 1: Read the CSV file containing the points
csv_path = 'tree_catastre_KL_raw/Baumstandorte_KL_epsg4326.csv'
points_df = pd.read_csv(csv_path, delimiter=";")
points_gdf = gpd.GeoDataFrame(points_df, geometry=gpd.points_from_xy(points_df.longitude, points_df.latitude), crs='EPSG:4326')

# # Step 1b: Read the shapefile
# shapefile_path = '/home/ruf/Documents/projects/koop_kl/Daten/Baumkataster/ShapeRLP/Gemarkungen_RLP.shp'
# data = gpd.read_file(shapefile_path)
# search_string = 'Kaiserslautern'
# kaiserlautern_shape = data[data['gemarkung'].str.contains(search_string)]

# Step 1c: Read the shapefile
shapefile_path = 'tree_catastre_KL_raw/FL Statistischer Bezirk.shp'
kl_shapes = gpd.read_file(shapefile_path)

# Create a new figure
fig, ax = plt.subplots()

# Step 2: Plot the TIFFs
folder_path = 'tree_catastre_KL_raw/'
files = glob.glob(os.path.join(folder_path, '*.tif'))

# tqdm is wrapped around the list of files
for file in tqdm(files, desc="Processing TIFFs"):
    with rasterio.open(file) as src:
        # Reproject the points to the TIFF's CRS
        crs = src.crs

        # Plot the TIFF
        show(src, ax=ax)

# Step 3: Plot the points on top of the TIFFs
points_gdf_reprojected = points_gdf.to_crs(crs)
points_gdf_reprojected.plot(ax=ax, markersize=10, color='red')

# # Step 4: Plot the shape on top of the TIFFs
# shape_gdf_reprojected = kaiserlautern_shape.to_crs(crs)
# shape_gdf_reprojected.plot(ax=ax, facecolor="none", edgecolor='green')

# Combine the statistical districts into a single polygon
merged_polygon = kl_shapes.unary_union

# Extract the exterior boundaries of the polygon
outer_boundary = merged_polygon.exterior

# Create a GeoDataFrame with the outermost boundary
boundary_gdf = gpd.GeoDataFrame(geometry=[outer_boundary], crs=crs)
boundary_gdf.plot(ax=ax, facecolor="none", edgecolor='blue')

plt.show()