import os
import glob
import math
import rasterio
from rasterio.windows import Window

input_folder = "/path/to/tiff/folder"
output_folder = "/path/to/output/folder"
tile_size = 128

# Create the output folder if it does not exist
os.makedirs(output_folder, exist_ok=True)

# Iterate through the TIFF files in the input folder
for input_geotiff in glob.glob(os.path.join(input_folder, "*.tif")):
    with rasterio.open(input_geotiff) as src:
        width, height = src.width, src.height
        crs = src.crs
        transform = src.transform
        count = src.count
        dtype = src.dtypes[0]

        # Calculate the number of tiles in each dimension
        num_tiles_x = width // tile_size
        num_tiles_y = height // tile_size

        # Iterate through the tiles
        for tile_x in range(num_tiles_x):
            for tile_y in range(num_tiles_y):
                # Calculate the window for the current tile
                x_start = tile_x * tile_size
                y_start = tile_y * tile_size
                window = Window(x_start, y_start, tile_size, tile_size)

                # Read the data for the current tile
                data = src.read(window=window)

                # Calculate the new transform for the current tile
                new_transform = rasterio.windows.transform(window, transform)

                # Create the output file path
                input_file_name = os.path.basename(input_geotiff)
                input_file_base, _ = os.path.splitext(input_file_name)
                output_file = os.path.join(output_folder, f"{input_file_base}_tile_{tile_x}_{tile_y}.tif")

                # Write the current tile to a new TIFF file
                with rasterio.open(
                    output_file,
                    "w",
                    driver="GTiff",
                    height=tile_size,
                    width=tile_size,
                    count=count,
                    dtype=dtype,
                    crs=crs,
                    transform=new_transform,
                ) as dst:
                    dst.write(data)