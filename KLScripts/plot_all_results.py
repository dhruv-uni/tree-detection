import matplotlib.pyplot as plt
from rasterio.plot import show
import glob
import geopandas as gpd
import numpy as np
import pandas as pd
import pickle
import rasterio
import os

from shapely.geometry import LineString

from tqdm import tqdm


def get_df(coords,):
    coords = np.array(coords)
    gdf = gpd.GeoDataFrame(geometry=gpd.points_from_xy(coords[:, 1], coords[:, 0]), crs='EPSG:4326')
    gdf_reprojected = gdf.to_crs(crs)
    return gdf_reprojected

# Step 1: Read the CSV file containing the points
csv_path = 'datasets/tree_catastre_KL_raw/Baumstandorte_KL_epsg4326.csv'
points_df = pd.read_csv(csv_path, delimiter=";")
points_gdf = gpd.GeoDataFrame(points_df, geometry=gpd.points_from_xy(points_df.longitude, points_df.latitude), crs='EPSG:4326')


with open('results_deepf_005.pickle', 'rb') as handle:
    results = pickle.load(handle)
#tp = np.array(results['tp_locs_projected'])
#tp_gdf = gpd.GeoDataFrame(geometry=gpd.points_from_xy(tp[:, 1], tp[:, 0]), crs='EPSG:4326')


# # Step 1b: Read the shapefile
# shapefile_path = '/home/ruf/Documents/projects/koop_kl/Daten/Baumkataster/ShapeRLP/Gemarkungen_RLP.shp'
# data = gpd.read_file(shapefile_path)
# search_string = 'Kaiserslautern'
# kaiserlautern_shape = data[data['gemarkung'].str.contains(search_string)]

# Step 1c: Read the shapefile
shapefile_path = 'datasets/tree_catastre_KL_raw/FL Statistischer Bezirk.shp'
kl_shapes = gpd.read_file(shapefile_path)

# Create a new figure
fig, ax = plt.subplots()

# Step 2: Plot the TIFFs
folder_path = 'datasets/tree_catastre_KL_raw/'
files = glob.glob(os.path.join(folder_path, '*.tif'))

# tqdm is wrapped around the list of files
for file in tqdm(files, desc="Processing TIFFs"):
    if "DOP20UTM_4085476.tif" not in file:
        continue
    with rasterio.open(file) as src:
        # Reproject the points to the TIFF's CRS
        crs = src.crs
        # Plot the TIFF
        show(src, ax=ax)
    #break

# Step 3: Plot the points on top of the TIFFs
points_gdf_reprojected = points_gdf.to_crs(crs)
#points_gdf_reprojected.plot(ax=ax, markersize=10, color='red')

tp_locs_projected = get_df(results['tp_locs_projected'])
tp_locs_projected.plot(ax=ax, markersize=100, color='green', marker='+')

tp_gt_locs_projected = get_df(results['tp_gt_locs_projected'])

fp_locs_projected = get_df(results['fp_locs_projected'])
fp_locs_projected.plot(ax=ax, markersize=50, color='green', marker='^', facecolors='none', edgecolors='yellow')

fn_locs_projected = get_df(results['fn_locs_projected'])
fn_locs_projected.plot(ax=ax, markersize=100, color='m', marker='v', edgecolors='k')

gt_locs_projected = get_df(results['gt_locs_projected'])
gt_locs_projected.plot(ax=ax, markersize=50, color='coral', marker='.')

lines = [LineString([t, g]) for t, g in zip(tp_locs_projected.geometry, tp_gt_locs_projected.geometry)]

# Create a new GeoDataFrame containing the LineString geometries
lines_gdf = gpd.GeoDataFrame(geometry=lines, crs=tp_locs_projected.crs)

# Plot the lines on the map
lines_gdf.plot(ax=ax, color='yellow')

# # Step 4: Plot the shape on top of the TIFFs
# shape_gdf_reprojected = kaiserlautern_shape.to_crs(crs)
# shape_gdf_reprojected.plot(ax=ax, facecolor="none", edgecolor='green')

# Combine the statistical districts into a single polygon
merged_polygon = kl_shapes.unary_union

# Extract the exterior boundaries of the polygon
outer_boundary = merged_polygon.exterior

# Create a GeoDataFrame with the outermost boundary
boundary_gdf = gpd.GeoDataFrame(geometry=[outer_boundary], crs=crs)
boundary_gdf.plot(ax=ax, facecolor="none", edgecolor='blue')

plt.show()