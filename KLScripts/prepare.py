import argparse
import os
from pathlib import Path

import rasterio
from rasterio.windows import Window
from shapely.geometry import box
import geopandas as gpd
import pandas as pd
from tqdm import tqdm
import numpy as np



def get_tiff_info(output_folder): 
    # if images have already been split
    tiff_info = []
    files = [file for file in os.listdir(output_folder) if file.endswith('.tif')]

    for i, file in enumerate(tqdm(files)):
        file_path = os.path.join(output_folder, file)
        with rasterio.open(file_path) as src:
            tiff_bbox = box(*src.bounds)
            tiff_info.append({
                'file': file,
                'bbox': tiff_bbox,
                'crs': src.crs,
                'transform': src.transform
            })

    return tiff_info


def get_tiff_info_and_split(folder_path, size, output_folder):
    tiff_info = []
    files = [file for file in os.listdir(folder_path) if file.endswith('.tif')]

    # Calculate the total number of splits
    total_splits = 0
    for file in files:
        file_path = os.path.join(folder_path, file)
        with rasterio.open(file_path) as src:
            total_splits += ((src.height + size - 1) // size) * ((src.width + size - 1) // size)

    progress_bar = tqdm(total=total_splits, desc="Splitting TIFFs and extracting info")

    for file in files:
        file_path = os.path.join(folder_path, file)
        with rasterio.open(file_path) as src:
            base_name = os.path.splitext(file)[0]
            profile = src.profile
            split_id = 0

            for row in range(0, src.height, size):
                for col in range(0, src.width, size):
                    window = rasterio.windows.Window(col_off=col, row_off=row, width=size, height=size)
                    split_array = src.read(window=window)

                    # Pad the last window if it's too small
                    if split_array.shape[1] != size or split_array.shape[2] != size:
                        padding = ((0, 0), (0, size - split_array.shape[1]), (0, size - split_array.shape[2]))
                        split_array = np.pad(split_array, padding, mode='constant', constant_values=0)

                    output_profile = profile.copy()
                    output_profile.update({
                        'height': size,
                        'width': size,
                        'transform': rasterio.windows.transform(window, src.transform)
                    })

                    output_file = output_folder / f"{base_name}_split_{split_id}.tif"
                    with rasterio.open(output_file, 'w', **output_profile) as dst:
                        dst.write(split_array)

                    split_bbox = box(*rasterio.windows.bounds(window, src.transform))
                    tiff_info.append({
                        'file': output_file.name,
                        'bbox': split_bbox,
                        'crs': src.crs,
                        'transform': output_profile['transform']
                    })

                    split_id += 1
                    progress_bar.update(1)

    progress_bar.close()
    return tiff_info


def read_points(csv_path):
    points_df = pd.read_csv(csv_path, delimiter=";")
    points_gdf = gpd.GeoDataFrame(points_df, geometry=gpd.points_from_xy(points_df.longitude, points_df.latitude),
                                  crs='EPSG:4326')
    return points_gdf


def find_points_in_tiffs2(points_gdf_reprojected, tiff_info):
    results = {}
    points_outside = []

    # Create a boolean mask to keep track of points that are outside all TIFFs
    outside_mask = np.ones(len(points_gdf_reprojected), dtype=bool)

    for tiff in tqdm(tiff_info):
        # Find all points that are within the TIFF's bounding box
        within_mask = points_gdf_reprojected.within(tiff['bbox'])
        if within_mask.any():
            if tiff['file'] not in results:
                results[tiff['file']] = []

            # Get pixel coordinates for all points within the TIFF's bounding box
            px, py = rasterio.transform.rowcol(tiff['transform'],
                                               points_gdf_reprojected[within_mask].geometry.x,
                                               points_gdf_reprojected[within_mask].geometry.y)

            results[tiff['file']].extend([{
                'point_id': i_row,
                'x': point.geometry.x,
                'y': point.geometry.y,
                'pixel_x': px[i],
                'pixel_y': py[i]
            } for i, (i_row, point) in enumerate(points_gdf_reprojected[within_mask].iterrows())])

            # Update the outside mask
            outside_mask &= ~within_mask

    # Find all points that are outside all TIFFs
    points_outside.extend([{
        'point_id': i,
        'x': point.geometry.x,
        'y': point.geometry.y
    } for i, point in points_gdf_reprojected[outside_mask].iterrows()])

    return results, points_outside

def find_points_in_tiffs(points_gdf_reprojected, tiff_info):
    results = {}
    points_outside = []

    for i, point in tqdm(points_gdf_reprojected.iterrows(), total=len(points_gdf_reprojected),
                         desc="Processing points"):
        is_inside_bbox = False
        for tiff in tiff_info:
            if point.geometry.within(tiff['bbox']):
                is_inside_bbox = True
                if tiff['file'] not in results:
                    results[tiff['file']] = []

                # Get pixel coordinates
                px, py = rasterio.transform.rowcol(tiff['transform'], point.geometry.x, point.geometry.y)

                results[tiff['file']].append({
                    'point_id': i,
                    'x': point.geometry.x,
                    'y': point.geometry.y,
                    'pixel_x': px,
                    'pixel_y': py
                })
                break

        if not is_inside_bbox:
            points_outside.append({
                'point_id': i,
                'x': points_gdf.iloc[i].geometry.x,
                'y': points_gdf.iloc[i].geometry.y
            })

    return results, points_outside


def save_results_to_csv(results, points_outside, output_folder):
    if not output_folder.exists():
        os.makedirs(output_folder)

    for tiff_file, points in results.items():
        points_df = pd.DataFrame(points)
        output_file = os.path.join(output_folder, f'{os.path.splitext(tiff_file)[0]}_points.csv')
        points_df.to_csv(output_file, index=False)

    points_outside_df = pd.DataFrame(points_outside)
    points_outside_df.to_csv(os.path.join(output_folder, 'points_outside.csv'), index=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Find points in TIFF files.')
    parser.add_argument('folder_path', help='Path to the folder containing TIFF files.')
    parser.add_argument('csv_path', help='Path to the CSV file containing the points.')
    parser.add_argument('-o', '--output_folder', default='./output',
                        help='Path to the output folder (default: ./output).')
    parser.add_argument('-o_csv', '--output_folder_csv', default='./output',
                        help='Path to the output folder (default: ./output).')
    parser.add_argument('-s', '--size', type=int, default=256,
                        help='Size of the split TIFFs, must be a multiple of 32 (default: 32).')

    args = parser.parse_args()

    assert args.size % 32 == 0, "The provided size must be a multiple of 32."

    folder_path = args.folder_path
    csv_path = args.csv_path
    output_folder = Path(args.output_folder)
    output_folder_csv = Path(args.output_folder_csv)
    if not output_folder.exists():
        os.makedirs(output_folder)
    size = args.size

    points_gdf = read_points(csv_path)
    splitted_tiffs_info = get_tiff_info(output_folder) 
    #splitted_tiffs_info = get_tiff_info_and_split(folder_path, size, output_folder)
    points_gdf_reprojected = points_gdf.to_crs(splitted_tiffs_info[0]['crs'])

    results, points_outside = find_points_in_tiffs2(points_gdf_reprojected, splitted_tiffs_info)
    #results, points_outside = find_points_in_tiffs(points_gdf_reprojected, splitted_tiffs_info)
    save_results_to_csv(results, points_outside, output_folder_csv)


#/home/dhruv/miniconda3/envs/project/bin/python "/mnt/c/Users/dhruv/Documents/Uni/Master/Semester 3/Projekt/tree-detection/KLScripts/prepare.py" "tree_catastre_KL/"  "tree_catastre_KL/Baumstandorte_KL_epsg4326.csv"  -o "tree_catastre_KL_prep/images_out/" -o_csv "tree_catastre_KL_prep/csv/"