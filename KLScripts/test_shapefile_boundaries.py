import geopandas as gpd
import matplotlib.pyplot as plt

# Load the shapefile
shapefile_path = '/home/ruf/Documents/projects/koop_kl/Daten/Baumkataster/ShapeRLP/FL Statistischer Bezirk.shp'
data = gpd.read_file(shapefile_path)

# Combine the statistical districts into a single polygon
merged_polygon = data.unary_union

# Extract the exterior boundaries of the polygon
outer_boundary = merged_polygon.exterior

# Create a GeoDataFrame with the outermost boundary
boundary_gdf = gpd.GeoDataFrame(geometry=[outer_boundary])

# Plot the outermost boundary using GeoPandas
ax = boundary_gdf.plot(linestyle='-', linewidth=1)

# Add any additional visualization settings or decorations
ax.set_title('Outermost Borders of the City')
ax.set_aspect('equal')

# Display the plot
plt.show()





