
import os
import numpy as np
import pandas as pd
import torch
from PIL import Image
import pickle
import rasterio
from scipy.ndimage import distance_transform_edt
from torch.utils.data import DataLoader, Dataset
from torchvision import transforms

# create datasets, dataloaders etc.

class CustomAugment(object):
    def __call__(self, sample):
        image, gt_map = sample
        k = np.random.randint(4)
        image = transforms.functional.rotate(image, k * 90)
        gt_map = transforms.functional.rotate(gt_map, k * 90)
        if np.random.random() < 0.5:
            image = transforms.functional.hflip(image)
            gt_map = transforms.functional.hflip(gt_map)
        return image, gt_map

class TreeDataset(Dataset):
    def __init__(self, data_file, image_dir, csv_dir, train=True, meta_dir=None):
        self.data = pd.read_csv(data_file, header=None)
        self.image_dir = image_dir
        self.csv_dir = csv_dir
        self.train = train

        self.transform_aug = transforms.Compose([
            CustomAugment()
            ])

        self.transform_pre = transforms.Compose([
            transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
            ])

        image_index = [os.path.isfile(f"{self.image_dir}/{i}.tif") for i in self.data[0]]
        csv_index = [os.path.isfile(f"{self.csv_dir}/{i}.csv") for i in self.data[0]]
        index = [a and b for a, b in zip(image_index, csv_index)]
        self.data = self.data[index]

        self.crs = {}
        self.transform = {}
        self.meta = None
        if meta_dir:
            with open(meta_dir, 'rb') as f:
                self.meta = pickle.load(f)
        print("Dataset length: ", len(self.data))
    def __len__(self):
        return len(self.data)
    def __getitem__(self, idx):
        image_name = self.data.iloc[idx, 0]
        image_path = f"{self.image_dir}/{image_name}.tif"
        csv_path = f"{self.csv_dir}/{image_name}.csv"

        # check if the image and CSV files exist
        #if not os.path.exists(image_path) or not os.path.exists(csv_path):
            #return (torch.empty(0), torch.empty(0), torch.empty(0), torch.empty(0))

        image = Image.open(image_path)
        df = pd.read_csv(csv_path)
        if set(['pixel_x','pixel_y']).issubset(df.columns):
            coords = df[['pixel_y', 'pixel_x']].values
        else:
            coords = df.values
        width, height = image.size
        gt = torch.zeros((height, width), dtype=torch.float32)
        if len(coords) != 0:
            gt[coords[:,1],coords[:,0]] = 1
            distance = distance_transform_edt(1-gt.numpy()).astype('float32')
            confidence = np.exp(-distance**2/(2*3**2))

            confidence = torch.from_numpy(confidence)

            attention = confidence>0.001
            attention = attention.float()
        else:
            confidence = torch.zeros((height, width), dtype=torch.float32)
            attention = torch.zeros((height, width), dtype=torch.float32)

        gt_map = torch.cat((gt.unsqueeze(0), confidence.unsqueeze(0), attention.unsqueeze(0)), dim=0)

        if self.train:
            image, gt_map = self.transform_aug((image, gt_map))
        image = transforms.ToTensor()(image)[:3,:]
        image = self.transform_pre(image)
        if self.meta:
            crs = self.meta[image_name]['crs']
            transform = self.meta[image_name]['transform']
            return image, gt_map[0], gt_map[1], gt_map[2], crs, transform
        else:
            return image, gt_map[0], gt_map[1], gt_map[2]