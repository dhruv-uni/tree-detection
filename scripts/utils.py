import pandas as pd
from PIL import Image
import os

def create_patches(path,):
    # Load the csv file with tree locations
    tree_locations = pd.read_csv('tree_locations.csv')

    # Define patch size
    patch_size = 256

    # Open the tif image
    img = Image.open('image.tif')

    # Calculate the number of patches in x and y direction
    num_patches_x = img.width // patch_size
    num_patches_y = img.height // patch_size

    # Create a new column for adjusted x and y tree locations
    tree_locations['patch_x'] = tree_locations['x'] // patch_size
    tree_locations['patch_y'] = tree_locations['y'] // patch_size
    tree_locations['adjusted_x'] = tree_locations['x'] % patch_size
    tree_locations['adjusted_y'] = tree_locations['y'] % patch_size

    # Save the adjusted tree locations to a new csv file
    tree_locations.to_csv('adjusted_tree_locations.csv', index=False)

    # Create the patches and save them as separate image files
    for i in range(num_patches_x):
        for j in range(num_patches_y):
            patch = img.crop((i*patch_size, j*patch_size, (i+1)*patch_size, (j+1)*patch_size))
            patch.save(f'patch_{i}_{j}.tif')

def create_file_list(csv_path, images_path, output_path):
    # Get list of csv files
    csv_files = [os.path.splitext(f)[0].replace("_points", "") for f in os.listdir(csv_path) if f.endswith('.csv')]

    # Get list of tif files
    tif_files = [os.path.splitext(f)[0] for f in os.listdir(images_path) if f.endswith('.tif')]
    #print(tif_files)
    # Find intersection of the two lists
    intersected_list = list(set(csv_files) & set(tif_files))
    
    # Save intersected list to file
    with open(output_path, 'w') as f:
        for item in intersected_list:
            f.write(f"{item}\n")

#create_file_list("tree_catastre_KL/csv", "tree_catastre_KL/images", "tree_catastre_KL/test.txt")