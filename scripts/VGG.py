import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init
from torchvision import datasets, models, transforms



# https://github.com/pytorch/vision/blob/94c9417091ddf5b9a5105410a23e350a8dbb8997/torchvision/models/vgg.py#L63
def make_layers(cfg, batch_norm=False):
    layers = []
    in_channels = 5
    for v in cfg:
        if v == 'M':
            layers += [nn.MaxPool2d(kernel_size=2, stride=2)]
        else:
            conv2d = nn.Conv2d(in_channels, v, kernel_size=3, padding=1)
            if batch_norm:
                layers += [conv2d, nn.BatchNorm2d(v), nn.ReLU(inplace=True)]
            else:
                layers += [conv2d, nn.ReLU(inplace=True)]
            in_channels = v
    return nn.Sequential(*layers)


cfgs = {
    'A': [64, 'M', 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'B': [64, 64, 'M', 128, 128, 'M', 256, 256, 'M', 512, 512, 'M', 512, 512, 'M'],
    'D': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 'M', 512, 512, 512, 'M', 512, 512, 512, 'M'],
    'E': [64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512, 512, 'M', 512, 512, 512, 512, 'M'],
}


class VGG(nn.Module):
    def __init__(self,output_layers, channels=None):
        """ Initializes a custom VGG model.
            Arguments:
                output_layers: list of layers to output (0 for first layer, 1 for second layer, etc.)
        """
        super(VGG,self).__init__()
        self.output_layers = output_layers
        '''self.pool = nn.MaxPool2d(2, 2)
        self.conv1_1 = BaseConv(3, 64, 3, 1, activation='relu', use_bn=True)
        self.conv1_2 = BaseConv(64, 64, 3, 1, activation='relu', use_bn=True)
        self.conv2_1 = BaseConv(64, 128, 3, 1, activation='relu', use_bn=True)
        self.conv2_2 = BaseConv(128, 128, 3, 1, activation='relu', use_bn=True)
        self.conv3_1 = BaseConv(128, 256, 3, 1, activation='relu', use_bn=True)
        self.conv3_2 = BaseConv(256, 256, 3, 1, activation='relu', use_bn=True)
        self.conv3_3 = BaseConv(256, 256, 3, 1, activation='relu', use_bn=True)
        self.conv4_1 = BaseConv(256, 512, 3, 1, activation='relu', use_bn=True)
        self.conv4_2 = BaseConv(512, 512, 3, 1, activation='relu', use_bn=True)
        self.conv4_3 = BaseConv(512,512, 3, 1, activation='relu', use_bn=True)
        self.conv5_1 = BaseConv(512, 512, 3, 1, activation='relu', use_bn=True)
        self.conv5_2 = BaseConv(512, 512, 3, 1, activation='relu', use_bn=True)
        self.conv5_3 = BaseConv(512, 512, 3, 1, activation='relu', use_bn=True)'''

        self.features = models.vgg16(weights='IMAGENET1K_V1').features 
        if channels:
          self.features = self.change_weights(channels).features #make_layers(cfgs['D'], True) #
        

    def change_weights(self, channels):
        vgg_model = models.vgg16(weights='IMAGENET1K_V1')
        first_conv_layer = vgg_model.features[0]
        in_channels = channels
        out_channels = first_conv_layer.out_channels
        kernel_size = first_conv_layer.kernel_size


        #first_conv_layer = [nn.Conv2d(in_channels, 64, kernel_size=3, stride=1, padding=1, dilation=1, groups=1, bias=True)]
        #first_conv_layer.extend(list(vgg_model.features))  
        #vgg_model.features= nn.Sequential(*first_conv_layer )  
        #output = model(x)

        new_first_conv_layer = nn.Conv2d(in_channels, out_channels, kernel_size, stride=1, padding='same')

        # Copy the pre-trained weights for the first 3 channels
        with torch.no_grad():
            new_first_conv_layer.weight[:, :3] = first_conv_layer.weight
            new_first_conv_layer.bias = first_conv_layer.bias

        vgg_model.features[0] = new_first_conv_layer

        #self.features = vgg.features
        return vgg_model

    def load_pretrained_vgg(self):
        """ Load weights from the pre-trained VGG16 model. 
            This can only be called after the model has been built.
            Arguments:
                input_shape: input shape [H,W,C] (without the batch dimension)
        """

        # get pre-trained VGG for BGR input
        vgg_bgr = models.vgg16(weights='IMAGENET1K_V1').features
        
        # get weights in initial layer
        w_bgr,b_bgr = vgg_bgr[0].weight.data, vgg_bgr[0].bias.data

        
        # make new VGG with correct input shape
        vgg = models.vgg16(weights=None).features

        # copy in pre-trained weights to first layer
        w = vgg[0].weight.data
        w[:, :3, :, :] = w_bgr
        b = b_bgr
        vgg[0].weight.data = w
        vgg[0].bias.data = b
        
        # copy in pre-trained weights to remaining layers
        for i in range(1, len(vgg)):
            if isinstance(vgg[i], torch.nn.Conv2d):
                vgg[i].weight.data = vgg_bgr[i].weight.data
                vgg[i].bias.data = vgg_bgr[i].bias.data

        # copy weights to our layers
        def set_weights(layer, layer_in):
            layer.conv.weight.data = layer_in.weight.data
            layer.conv.bias.data = layer_in.bias.data

        set_weights(self.conv1_1, vgg[0])
        set_weights(self.conv1_2, vgg[2])
        set_weights(self.conv2_1, vgg[5])
        set_weights(self.conv2_2, vgg[7])
        set_weights(self.conv3_1, vgg[10])
        set_weights(self.conv3_2, vgg[12])
        set_weights(self.conv3_3, vgg[14])
        set_weights(self.conv4_1, vgg[17])
        set_weights(self.conv4_2, vgg[19])
        set_weights(self.conv4_3, vgg[21])
        set_weights(self.conv5_1, vgg[24])
        set_weights(self.conv5_2, vgg[26])
        set_weights(self.conv5_3, vgg[28])

    def forward2(self,inputs):

        x = inputs
        l = []

        x = self.conv1_1(x) # 0
        l.append(x)
        x = self.conv1_2(x) # 1
        l.append(x)
        x = self.pool(x)

        x = self.conv2_1(x) # 2
        l.append(x)
        x = self.conv2_2(x) # 3
        l.append(x)
        x = self.pool(x)

        x = self.conv3_1(x) # 4
        l.append(x)
        x = self.conv3_2(x) # 5
        l.append(x)
        x = self.conv3_3(x) # 6
        l.append(x)
        x = self.pool(x)

        x = self.conv4_1(x) # 7
        l.append(x)
        x = self.conv4_2(x) # 8
        l.append(x)
        x = self.conv4_3(x) # 9
        l.append(x)
        x = self.pool(x)

        x = self.conv5_1(x) # 10
        l.append(x)
        x = self.conv5_2(x) # 11
        l.append(x)
        x = self.conv5_3(x) # 12
        l.append(x)
        
        return tuple(l[i] for i in self.output_layers)

    def forward(self, x):
        l = []
        for layer in self.features:
            x = layer(x)
            if isinstance(layer, nn.Conv2d):
                l.append(x)
        return tuple(l[i] for i in self.output_layers)
    
class BaseConv(nn.Module):
    def __init__(self, in_channels, out_channels, kernel, stride=1, activation=None, use_bn=False):
        super(BaseConv,self).__init__()
        self.use_bn = use_bn
        self.conv = nn.Conv2d(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel, stride=stride, padding='same',)
        #init.normal_(self.conv.weight, mean=0.0, std=0.01)
                           #kernel_initializer=initializers.RandomNormal(stddev=0.01))
        self.bn = nn.BatchNorm2d(out_channels)
        
        if activation is None:
            self.activation = None
        else:
            if activation == 'relu':
                self.activation = nn.ReLU()
            else:
                self.activation = nn.Sigmoid()
    
    def forward(self,inputs):
        x = self.conv(inputs)
        if self.use_bn:
            x = self.bn(x)
        if self.activation:
            x = self.activation(x)
        return x
