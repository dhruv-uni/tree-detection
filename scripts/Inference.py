
import numpy as np
from PIL import Image

import argparse
import os
import sys

import rasterio
import rasterio.transform

from skimage.feature import peak_local_max

import tempfile

import geopandas as gpd

import torch
from torchvision import transforms
#import tensorflow as tf

def preprocess_image(image_path):
    transform_pre = transforms.Compose([
    transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
    ])
    image = Image.open(image_path)
    image = transforms.ToTensor()(image)
    image = transform_pre(image)
    return image

def unprocess_image(image):
    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]
    image = image.cpu() * torch.tensor(std).view(3, 1, 1) + torch.tensor(mean).view(3, 1, 1)
    return image.permute(1,2,0).cpu()

def unprocess_image_batch(images):
    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]
    images = images.cpu() * torch.tensor(std).view(1, 3, 1, 1) + torch.tensor(mean).view(1, 3, 1, 1)
    return images.permute(0,2,3,1).cpu()

def predict_image(model, image_path, device, thresholds = [1, None, 0.05]):
    min_distance = thresholds[0]
    threshold_abs=thresholds[1] 
    threshold_rel=thresholds[2]
    
    image = preprocess_image(image_path)
    image = image.unsqueeze(0).to(device)
    with torch.no_grad():
        prediction, _ = model(image)
    prediction = prediction[0,0].cpu().numpy()
    indices = peak_local_max(prediction, min_distance=min_distance, threshold_abs=threshold_abs, threshold_rel=threshold_rel)

    return indices, unprocess_image(image[0])

def preprocess(image):
  input_images = tf.keras.applications.vgg16.preprocess_input(image)
  input_images = torch.tensor(input_images.copy())
  #input_images = torch.tensor(np.concatenate([input_images,input_images[:,:,:,:2]],axis=-1))
  input_images = input_images.permute(0, 3, 1, 2)
  return input_images

def _tiled_inference(model,input_path,output_path,tile_size,overlap, device):
    print(10000)
    transform_pre = transforms.Compose([
    transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
    ])
    with rasterio.open(input_path,'r') as src:
        meta = src.meta
        height = meta['height']
        width = meta['width']
        
        padded_size = tile_size+overlap*2
        
        meta['count'] = 1
        meta['dtype'] = 'float32'
        with rasterio.open(output_path,'w',**meta) as dest:
        
            for row in range(overlap,height-overlap,tile_size):
                for col in range(overlap,width-overlap,tile_size):
                    window = rasterio.windows.Window(col-overlap,row-overlap,padded_size,padded_size)
                    image = src.read([1,2,3],window=window)
                    image = np.expand_dims(np.transpose(image,[1,2,0]),axis=0)
                    
                    down_pad = max(0,padded_size-image.shape[1])
                    right_pad = max(0,padded_size-image.shape[2])
                    image = np.pad(image,((0,0),(0,down_pad),(0,right_pad),(0,0)))
                    
                    input_images = transforms.ToTensor()(image[0,:])

                    input_images = input_images.unsqueeze(0)
                    input_images = transform_pre(input_images)
                    input_images = input_images.to(device)

                    with torch.no_grad():
                      output, _ = model(input_images)
                      output = output.permute(0,2,3,1).cpu().numpy()
                    
                    # zero out "no data" pixels
                    #mask = np.all(image==nodata,axis=-1)
                    #output[mask] = 0

                    output_crop = output[0,overlap:-overlap,overlap:-overlap,0]

                    h = min(height-row,output_crop.shape[0])
                    w = min(width-col,output_crop.shape[1])
                    window = rasterio.windows.Window(col,row,w,h)
                    dest.write(output_crop[None,:h,:w],window=window)

def _tiled_peak_finding(path,input_size,overlap,min_distance,threshold_abs,threshold_rel):
    with rasterio.open(path,'r') as f:
        meta = f.meta
        height = meta['height']
        width = meta['width']
        
        padded_size = input_size+overlap*2
        
        all_indices = []
        
        for row in range(overlap,height-overlap,input_size):
            for col in range(overlap,width-overlap,input_size):
                window = rasterio.windows.Window(col-overlap,row-overlap,padded_size,padded_size)
                image = np.squeeze(f.read(1,window=window))
                
                indices = peak_local_max(image,min_distance=min_distance,threshold_abs=threshold_abs,threshold_rel=threshold_rel)
                
                good = np.all(np.stack([
                    indices[:,0] >= overlap,
                    indices[:,0] < overlap+input_size,
                    indices[:,1] >= overlap,
                    indices[:,1] < overlap+input_size],
                    axis=-1),axis=-1)
                indices = indices[good]
                indices[:,0] += row-overlap
                indices[:,1] += col-overlap
                
                all_indices.append(indices)
        all_indices = np.concatenate(all_indices,axis=0)
        return all_indices


def run_tiled_inference(model,input_path,output_path, min_distance,threshold_abs,threshold_rel, device, tile_size, overlap):

    temp_path = tempfile.NamedTemporaryFile(suffix='.tif').name

    _tiled_inference(
        model=model,
        input_path=input_path,
        output_path=temp_path,
        tile_size=tile_size,
        overlap=overlap,
        device=device)

    with rasterio.open(temp_path,'r') as f:
        meta = f.meta
        epsg = meta['crs'].to_epsg()
        crs = f'EPSG:{epsg}'
        transform = meta['transform']

    indices = _tiled_peak_finding(temp_path,input_size=tile_size,overlap=overlap,min_distance=min_distance,threshold_abs=threshold_abs,threshold_rel=threshold_rel)

    #x,y = rasterio.transform.xy(transform,indices[:,0],indices[:,1])
    #print(x,y)
    #
    #gdf = gpd.GeoDataFrame(geometry=gpd.points_from_xy(x,y),crs=crs)
    #gdf.to_file(output_path,driver='GeoJSON')
    os.remove(temp_path)

    return indices

    