import torch
import torch.nn as nn
from tqdm import tqdm


def train_model(model, train_loader, val_loader, num_epochs, lr, model_path, device):

    optimizer = torch.optim.Adam(model.parameters(), lr=lr)

    mse_loss = nn.MSELoss()
    bce_loss = nn.BCELoss()

    train_losses = []
    val_losses = []
    best_val_loss = float('inf')

    pbar = tqdm(range(num_epochs), desc="Epoch")
    for epoch in pbar:
        train_loss = 0
        for batch_idx, (images, _, confidence, attention) in enumerate(train_loader): 
            model.train()   

            images = images.to(device)
            confidence = confidence.to(device).unsqueeze(1)
            attention = attention.to(device).unsqueeze(1)

            confidence_pred, attention_pred = model(images)

            loss = mse_loss(confidence_pred, confidence) + 0.1*bce_loss(attention_pred, attention)
            train_loss += loss.item()

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            pbar.set_description(f"Epoch: {epoch} Batch: {batch_idx}/{len(train_loader)} Loss: {loss.item()}")
        
        train_loss /= len(train_loader)
        train_losses.append(train_loss)

        #validation
        model.eval()
        val_loss = 0
        with torch.no_grad():
            for batch_idx, (images, _, confidence, attention) in enumerate(val_loader): 
                images = images.to(device)
                confidence = confidence.to(device).unsqueeze(1)
                attention = attention.to(device).unsqueeze(1)

                confidence_pred, attention_pred = model(images)
                val_loss += (mse_loss(confidence_pred, confidence) + 0.1*bce_loss(attention_pred, attention)).item()

        val_loss /= len(val_loader)
        val_losses.append(val_loss)

        # save model
        if val_loss < best_val_loss:
            best_val_loss = val_loss
            torch.save(model.state_dict(), model_path)
    return train_losses, val_losses