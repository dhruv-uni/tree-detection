import torch
import torch.nn as nn
import torch.nn.functional as F
from .VGG import *

class BackEnd(torch.nn.Module):
    def __init__(self,half_res=False):
        super(BackEnd,self).__init__()
        self.half_res = half_res

        self.upsample = nn.Upsample(scale_factor=2, mode='bilinear')
        self.conv1 = BaseConv(1024, 256, 1, 1, activation='relu', use_bn=True)
        self.conv2 = BaseConv(256, 256, 3, 1, activation='relu', use_bn=True)

        self.conv3 = BaseConv(512, 128, 1, 1, activation='relu', use_bn=True)
        self.conv4 = BaseConv(128, 128, 3, 1, activation='relu', use_bn=True)

        self.conv5 = BaseConv(256, 64, 1, 1, activation='relu', use_bn=True)
        self.conv6 = BaseConv(64, 64, 3, 1, activation='relu', use_bn=True)
        self.conv7 = BaseConv(64, 32, 3, 1, activation='relu', use_bn=True)

        if not self.half_res:
            self.conv8 = BaseConv(96, 32, 1, 1, activation='relu', use_bn=True)
            self.conv9 = BaseConv(32, 32, 3, 1, activation='relu', use_bn=True)
            self.conv10 = BaseConv(32, 32, 3, 1, activation='relu', use_bn=True)
    
    def forward(self,inputs):
        if self.half_res:
            conv2_2, conv3_3, conv4_3, conv5_3 = inputs
        else:
            conv1_2, conv2_2, conv3_3, conv4_3, conv5_3 = inputs

        #print(conv1_2.shape, conv2_2.shape, conv3_3.shape, conv4_3.shape, conv5_3.shape)

        x = self.upsample(conv5_3)

        x = torch.cat([x, conv4_3], dim=1)
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.upsample(x)

        x = torch.cat([x, conv3_3], dim=1) 
        x = self.conv3(x)
        x = self.conv4(x)
        x = self.upsample(x)

        x = torch.cat([x, conv2_2], dim=1)
        x = self.conv5(x)
        x = self.conv6(x)
        x = self.conv7(x)
        
        if not self.half_res:
            x = self.upsample(x)
            x = torch.cat([x, conv1_2], dim=1)
            x = self.conv8(x)
            x = self.conv9(x)
            x = self.conv10(x)

        return x




class SFANet(torch.nn.Module):
    def __init__(self, half_res=True, channels=None):
        super(SFANet,self).__init__()
        output_layers = [3,6,9,12] if half_res else [1,3,6,9,12]
        self.vgg = VGG(output_layers=output_layers, channels=channels)
        self.amp = BackEnd(half_res=half_res)
        self.dmp = BackEnd(half_res=half_res)
        
        self.conv_att = BaseConv(32, 1, 1, 1, activation='sigmoid', use_bn=True)
        self.conv_out = BaseConv(32, 1, 1, 1, activation=None, use_bn=False)
    
    def forward(self,inputs):
        #print("start shape: sfanet")
        #print(inputs.shape)
        x = inputs
        #print(x.shape)
        x = self.vgg(x)
        amp_out = self.amp(x)
        #print(amp_out.shape)
        dmp_out = self.dmp(x)
        #print(dmp_out.shape)

        amp_out = self.conv_att(amp_out)
        #print(amp_out.shape)
        #dmp_out = torch.matmul(amp_out, dmp_out)
        #print(dmp_out.shape)
        dmp_out = amp_out * dmp_out
        dmp_out = self.conv_out(dmp_out)
        #print(dmp_out.shape)
        
        return dmp_out, amp_out

class Model(torch.nn.Module):
    def __init__(self, half_res=False, channels=None):
        super(Model, self).__init__()
        self.sfanet = SFANet(half_res=half_res, channels=channels)

    def forward(self, x):
        dmp, amp = self.sfanet(x)
        return (dmp, amp)

def build_model(half_res=False, channels=None):
    training_model = Model(half_res, channels)
    #testing_model = Model()
    return training_model #, testing_model
