
import numpy as np
import torch
from skimage.feature import peak_local_max
from .evaluate import evaluate_batch, calculate_scores
from tqdm import tqdm
from matplotlib import pyplot as plt
from .Inference import unprocess_image, unprocess_image_batch
from matplotlib.lines import Line2D
import math

def test_model(model, test_loader, device, min_distance, threshold_abs, threshold_rel, return_locs=True):
    all_tp = 0
    all_fp = 0
    all_fn = 0
    all_tp_dists = []

    all_tp_locs = []
    all_tp_gt_locs = []
    all_fp_locs = []
    all_fn_locs = []
    all_gt_locs = []

    all_tp_locs_projected = []
    all_tp_gt_locs_projected = []
    all_fp_locs_projected = []
    all_fn_locs_projected = []
    all_gt_locs_projected = []

    for batch_idx, data in enumerate(tqdm(test_loader)): 
        if len(data) == 6:
            images, test_gts, confidence, attention, crs, transforms = data
        else:
            images, test_gts, confidence, attention = data
            crs = None
            transforms = None
        images = images.to(device)

        with torch.no_grad():
            confidence_pred, _ = model(images)

        confidence_pred = confidence_pred.permute(0,2,3,1)[:,:,:,0].cpu().numpy()
  
        batch_results = evaluate_batch(
            gts=test_gts,
            preds=confidence_pred,
            min_distance=min_distance,
            threshold_rel=threshold_rel,
            threshold_abs=threshold_abs,
            max_distance=20,
            return_locs=return_locs,
            crs=list(crs) if crs else None,
            transforms=transforms.cpu().numpy() if transforms else None)
        
        if return_locs and transforms:
            tp, fp, fn, tp_dists, tp_locs, tp_gt_locs, fp_locs, fn_locs, gt_locs, tp_locs_projected, tp_gt_locs_projected, fp_locs_projected, fn_locs_projected, gt_locs_projected = batch_results
        elif return_locs:
            tp, fp, fn, tp_dists, tp_locs, tp_gt_locs, fp_locs, fn_locs, gt_locs, *rest = batch_results
        else:
            tp, fp, fn, tp_dists, tp_locs, *rest = batch_results

        all_tp += tp
        all_fp += fp
        all_fn += fn
        
        all_tp_dists += tp_dists

        if return_locs:
            all_tp_locs += tp_locs
            all_tp_gt_locs += tp_gt_locs
            all_fp_locs += fp_locs
            all_fn_locs += fn_locs
            all_gt_locs += gt_locs
            if transforms:
                all_tp_locs_projected += tp_locs_projected
                all_tp_gt_locs_projected += tp_gt_locs_projected
                all_fp_locs_projected += fp_locs_projected
                all_fn_locs_projected += fn_locs_projected
                all_gt_locs_projected += gt_locs_projected

    # all_tp_dists = np.concatenate(all_tp_dists)
    results = calculate_scores(all_tp, all_fp, all_fn, all_tp_dists)

    if return_locs:
        results.update({
            'tp_locs':all_tp_locs,
            'tp_gt_locs':all_tp_gt_locs,
            'fp_locs':all_fp_locs,
            'fn_locs':all_fn_locs,
            'gt_locs':all_gt_locs,
            'tp_locs_projected':all_tp_locs_projected,
            'tp_gt_locs_projected':all_tp_gt_locs_projected, 
            'fp_locs_projected':all_fp_locs_projected,
            'fn_locs_projected':all_fn_locs_projected, 
            'gt_locs_projected':all_gt_locs_projected
        })
    print()
    print('------- results for: ' + "testing" + ' ---------')
    print('precision: ',results['precision'])
    print('recall: ',results['recall'])
    print('fscore: ',results['fscore'])
    print('rmse [px]: ',results['rmse'])

    return results

def plot_test_data(model, test_dataset, device, min_distance, threshold_abs, threshold_rel, n_samples=4, seed=1234):
    np.random.seed(seed)
    N = len(test_dataset)
    indices = np.random.choice(N, size=n_samples, replace=False)

    images = []
    gt_maps = []
    gt_confidences = []
    gt_attentions = []
    for i in indices:
        out = test_dataset[i]
        image = out[0]
        gt_map = out[1]
        gt_confidence = out[2]
        gt_attention = out[3]
        # image, gt_map, gt_confidence, gt_attention = test_dataset[i]
        images.append(image)
        gt_maps.append(gt_map)
        gt_confidences.append(gt_confidence)
        gt_attentions.append(gt_attention)

    # Convert lists to tensors
    images = torch.stack(images)
    gt_maps = torch.stack(gt_maps)
    gt_confidences = torch.stack(gt_confidences)
    gt_attentions = torch.stack(gt_attentions)


    images = images.to(device)
    with torch.no_grad():
        confidence_pred, _ = model(images)
    confidence_pred = confidence_pred.permute(0,2,3,1)[:,:,:,0].cpu().numpy()

    tp, fp, fn, tp_dists, tp_locs, tp_gt_locs, fp_locs, fn_locs, gt_locs = evaluate_batch(
        gts=gt_maps,
        preds=confidence_pred,
        min_distance=min_distance,
        threshold_rel=threshold_rel,
        threshold_abs=threshold_abs,
        max_distance=20,
        return_locs=True)

    images_unpr = unprocess_image_batch(images)
    fig,ax = plt.subplots(math.isqrt(n_samples),math.isqrt(n_samples),figsize=(8.5,11),tight_layout=True)
    for a in ax.flatten(): a.axis('off')
    for a,im,tp,tp_gt,fp,fn,gt in zip(ax.flatten(),images_unpr,
                        tp_locs,tp_gt_locs,fp_locs,fn_locs,gt_locs):
        a.imshow(im)

        if len(gt)>0:
            if len(gt.shape)==1: gt = gt[None,:]
            a.plot(gt[:,0],gt[:,1],'m.')
        
        if len(tp)>0:
            if len(tp.shape)==1: tp = tp[None,:]
            a.plot(tp[:,0],tp[:,1],'g+', markeredgecolor='green')

        if len(fp)>0:
            if len(fp.shape)==1: fp = fp[None,:]
            #a.plot(fp[:,0],fp[:,1],'y^', facecolors='none')
            a.scatter(fp[:, 0], fp[:, 1], marker='^', facecolors='none', edgecolors='y')

        if len(fn)>0:
            if len(fn.shape)==1: fn = fn[None,:]
            a.plot(fn[:,0],fn[:,1],'m.',markeredgecolor='k',markeredgewidth=1)
        
        if len(tp_gt)>0:
            if len(tp_gt.shape)==1: tp_gt= tp_gt[None,:]
            for t,g in zip(tp,tp_gt):
                a.plot((t[0],g[0]),(t[1],g[1]),'y-')
    # Create custom legend handles
    legend_elements = [
        Line2D([0], [0], marker='o', color='w', label='Ground Truth', markerfacecolor='m', markersize=10),
        Line2D([0], [0], marker='+', color='w', label='True Positive', markeredgecolor='g', markersize=10),
        Line2D([0], [0], marker='^', color='w', label='False Positive', markerfacecolor='y', markersize=10),
        Line2D([0], [0], marker='o', color='w', label='False Negative', markerfacecolor='m', markeredgecolor='k', markersize=10),
        Line2D([0], [0], color='y', label='TP-GT')
    ]

    # Add legend to the last subplot
    fig.legend(handles=legend_elements, bbox_to_anchor=(-0.2, 0.95), loc='upper left')   

    return fig